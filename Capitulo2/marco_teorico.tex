
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%           Capítulo 2: MARCO TEÓRICO - REVISIÓN DE LITERATURA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter{Marco teórico}
\noindent En este capítulo se se aborda la descripción de temas y términos que son importantes para la comprensión del presente trabajo de tesis. Se cuenta con una sección dedicada a la definición de conceptos básicos sobre pronóstico de series de tiempo, métodos estadisticos de pronóstico así como también metodos computacionales de Inteligencia Artificial. Se abordan descripciones de los distintos métodos de clasificación que son utilizados para la realización de pronóstico los cuales forman la base de la metodología de pronóstico presentada en este trabajo de investigación.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Pronóstico de series de tiempo -- Metodología %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Pronóstico de Series de Tiempo}

El análisis de información orientada al tiempo y el pronóstico de valores futuros de una serie de tiempo figuran entre los problemas más importantes que los analistas enfrentan en muchos campos, desde las finanzas y la economía hasta la gestión de las operaciones de producción. El pronóstico es un problema importante que abarca muchos campos, incluyendo los negocios, la industria, el gobierno, la economía, las ciencias ambientales, la medicina, la ciencias sociales, la política y las finanzas. Los problemas de pronóstico a menudo se clasifican como de corto, mediano y largo plazo.\\

Los problemas de pronóstico a corto plazo implican predecir eventos sólo unos pocos períodos de tiempo (días, semanas, meses) en el futuro. Los pronósticos a medio plazo se extienden de uno a dos años en el futuro, y los problemas de pronóstico a largo plazo pueden extenderse más allá de muchos años. Se requieren pronósticos a corto y mediano plazo para actividades que van desde la gestión de operaciones hasta la elaboración de presupuestos y la selección de nuevos proyectos de investigación y desarrollo.\cite{montgomery2015introduction}. La mayoría de estos problemas de pronóstico involucran el uso de datos a manera de series de tiempo. A continuación se definen algunos de los conceptos más importantes en lo que respecta al análisis y pronóstico de series de tiempo.\\

\noindent \textbf{Pronóstico.} Es una predicción de algún evento o eventos futuros. Como sugiere Neils Bohr, hacer buenas predicciones no siempre es fácil. Por este motivo la obtención de un buen pronóstico no siempre es efectiva. Entre muchos famosos "malos" pronósticos encontramos los siguientes del libro Bad Predictions \cite{lee2000bad}:

\begin{itemize}
  \item "La población es constante en tamaño y permanecerá hasta el final de la humanidad." L'Encyclopedie, 1756.
  \item "1930 será un espléndido año de empleo ". Departamento de Trabajo de los Estados Unidos, Pronóstico de Año Nuevo en 1929, justo antes de la caída del mercado el 29 de octubre.
  \item "Las computadoras se multiplican a un ritmo rápido. Para el cambio de siglo habrá 220.000 en los EE.UU." Wall Street Journal, 1966.
\end{itemize}

\noindent \textbf{Serie de Tiempo.} Es una secuencia de observaciones de una variable de interes en el tiempo\cite{montgomery2015introduction}. Una definición más es la propuesta por Chatfield en su libro \textit{Time-series forecasting} \cite{chatfield2000time} la cual nos dice que una serie de tiempo es un conjunto de observaciones medidas secuencialmente a través del tiempo. Estas mediciones pueden realizarse continuamente a lo largo del tiempo o tomarse en un conjunto discreto de puntos de tiempo. Por convención, estos dos tipos de series se denominan series de tiempo continuo y discreto, respectivamente, aunque la variable medida puede ser discreta o continua en ambos casos. En otras palabras, para series de tiempo discretas, por ejemplo, es el eje de tiempo que es discreto. Un ejemplo de serie tiempo se puede apreciar en la figura \ref{ts_example} donde es posible observar la lectura de la velocidad de viento registrada en una estación de viento en un rango de tiempo determinado, los valores de la magnitud se encuentran normalizados.\\

\begin{figure}[h]
  \centering
    \includegraphics[scale=0.65]{Capitulo2/figs/timeseries_example.pdf}
  \caption{Ejemplo de serie de tiempo, muestra los valores normalizados de la lectura de la velocidad de viento registrada en una estación de viento.}            %Pie de imagen
  \label{ts_example}                              %nombre de referencia
\end{figure}

La razón de que la predicción sea tan importante es que la predicción de eventos futuros es un aporte crítico en muchos tipos de procesos de planificación y toma de decisiones. Con aplicación a áreas como las siguientes:

\begin{enumerate}
  \item \textbf{Gestión de Operaciones.} Las organizaciones empresariales utilizan rutinariamente los pronósticos de ventas de productos o la demanda de servicios para programar la producción. Controlar los inventarios, administrar la cadena de suministro, determinar las necesidades de personal. Y planificar la capacidad. Los pronósticos también pueden utilizarse para determinar la mezcla de productos o servicios que se ofrecerán y los lugares en los que se van a producir los productos.
  \item \textbf{Mercadotecnia.} La predicción es importante en muchas decisiones de mercadotecnia. Los pronósticos de respuesta de las ventas a los gastos de publicidad, nuevas promociones o cambios en las políticas de precios permiten a las empresas evaluar su efectividad, determinar si se están cumpliendo los objetivos y hacer ajustes.
  \item \textbf{Finanzas y Gestión de Riesgos}. Los inversionistas en activos financieros están interesados en pronosticar los beneficios de sus inversiones. Estos activos incluyen, pero no se limitan a acciones, bonos y materias primas; Otras decisiones de inversión pueden hacerse en relación con los pronósticos de tipos de interés, opciones y tipos de cambio de moneda. La gestión del riesgo financiero requiere pronósticos de la volatilidad de los rendimientos de los activos de manera que los riesgos asociados con las carteras de inversión puedan ser evaluados y asegurados, y que los derivados financieros puedan tener un precio adecuado.
  \item \textbf{Ciencias económicas.} Los gobiernos, las instituciones financieras y las organizaciones de políticas requieren pronósticos de las principales variables económicas, como el producto interno bruto, el crecimiento demográfico, el desempleo, los tipos de interés, la inflación, el crecimiento del empleo, la producción y el consumo. Estos pronósticos forman parte integral de la orientación de la política monetaria y fiscal y de los planes y decisiones presupuestarios de los gobiernos. También son fundamentales en las decisiones de planificación estratégica tomadas por las organizaciones empresariales y las instituciones financieras.
  \item \textbf{Control de Procesos Industriales.} Los pronósticos de los valores futuros de las características críticas de la calidad de un proceso de producción pueden ayudar a determinar cuándo deben cambiarse las variables controlables importantes en el proceso, o si el proceso debe ser cerrado y revisado. La retroalimentación y el control directo son ampliamente utilizados en el monitoreo y ajuste de procesos industriales, y las predicciones de la salida del proceso son una parte integral de estos esquemas.
  \item \textbf{Demografía.} Los pronósticos de población por país y regiones se hacen en forma rutinaria, a menudo estratificadas por variables como el género, la edad y la raza. Los demógrafos también pronostican los nacimientos, las muertes y los patrones migratorios de las poblaciones. Los gobiernos usan estas pronósticos para planificar políticas y acciones de servicios sociales, tales como gastos en salud, programas de jubilación y programas de lucha contra la pobreza. Muchas empresas utilizan los pronósticos de las poblaciones por grupos de edad para hacer planes estratégicos en relación con el desarrollo de nuevas líneas de productos o los tipos de servicios que se ofrecerán.
\end{enumerate}

Estas son sólo algunas de las muchas situaciones diferentes en las que se requieren pronósticos para tomar buenas decisiones. A pesar de la amplia gama de situaciones problemáticas que requieren pronósticos, sólo hay dos tipos de métodos de pronóstico: métodos \textbf{cualitativos} y métodos \textbf{cuantitativos}.\\

\noindent \textbf{Cualitativos.}
Este tipo de técnicas de pronóstico son con frecuencia de naturaleza subjetiva y requieren juicio por parte de los expertos en el área que se está analizando. Los pronósticos cualitativos se usan con frecuencia en situaciones donde hay pocos o ningún dato histórico sobre el cual basar el pronóstico. \\
Un ejemplo sería la introducción de un producto nuevo en el mercado, para el cual no hay historia relevante. En esta situación, la empresa podría utilizar la opinión experta del personal de ventas y marketing para estimar subjetivamente las ventas de productos durante la fase de introducción de nuevos productos de su ciclo de vida. A veces los métodos de predicción cualitativa hacen uso de pruebas de marketing, encuestas de clientes potenciales y el uso de experiencia con el rendimiento de ventas de otros productos (tanto sus propios como los de los competidores). Sin embargo. Aunque puede realizarse algún análisis de datos, la base del pronóstico es el juicio subjetivo.\\

\noindent \textbf{Cuantitativos.}
Las técnicas de predicción cuantitativa hacen uso formal de datos históricos y un modelo de predicción. El modelo resume formalmente los patrones en los datos y expresa una relación estadística entre los valores anteriores y los actuales de la variable. A continuación, el modelo se utiliza para proyectar los patrones de los datos en el futuro. En otras palabras. El modelo de pronóstico se utiliza para extrapolar el comportamiento pasado y actual en el futuro.\\

Normalmente se piensa en un pronóstico como un número único que representa la mejor estimación del valor futuro de la variable de interés. Los estadísticos llamarían a esto una \textbf{estimación de punto} o \textbf{pronóstico de punto}. Sin embargo, estos pronósticos casi siempre son erróneos; Es decir, se experimenta un \textbf{error de pronóstico}. Por consiguiente. Generalmente es una buena práctica acompañar un pronóstico con una estimación de cuán grande podría ser un error de pronóstico. Una forma de hacerlo es proporcionar un \textbf{intervalo de predicción} (PI) para acompañar la predicción de puntos.\\
%El IP es un rango de valores para la observación futura, y es probable que resulte mucho más útil en la toma de decisiones que un solo número.

\noindent \textbf{Error de pronóstico.} Una definición simple es la que se encuentra en The forecasting dictionary\cite{armstrong2000forecasting} la cual lo especifica como la diferencia entre los valores reales y los valores pronosticados. Sin embargo, existen distintas maneras de medir el error en los pronósticos y con esto determinar la precisión del pronóstico generado.\\

\noindent Algunas de las medidas más relevantes para la evaluación en el pronóstico son las siguientes:

\textbf{Error Cuadrático Medio (MSE).} Es un estimador que mide el promedio de los errores al cuadrado, en otras palabras el error promedio al ignorar los signos. Su representación matemática se observa en la Ecuación \ref{Eq:MSE}.

\begin{equation}
\begin{aligned}
  MSE = \frac{1}{n} \sum_{i=1}^{n} (\hat{y}_{i}-y_{i})^{2}
\end{aligned}
    \label{Eq:MSE}
\end{equation}

\textbf{Error Cuadrático Medio (RMSE).} Es la raíz cuadrada de la sumatoria de los errores cuadráticos. Depende de la escala de la variable dependiente. Debe utilizarse como medida relativa para comparar los pronósticos de la misma serie en diferentes modelos. Su representación matemática se observa en la Ecuación \ref{Eq:MSE}.

\begin{equation}
\begin{aligned}
  RMSE = \sqrt{RMSE}
\end{aligned}
    \label{Eq:RMSE}
\end{equation}

\textbf{Error Absoluto Medio (MAE).} El  MAE también depende de la escala de la variable dependiente, pero es menos sensible a grandes desviaciones que la pérdida cuadrática habitual. Su representación matemática se observa en la Ecuación \ref{Eq:MAE}.

\begin{equation}
\begin{aligned}
  MAE = \frac{1}{n} \sum^{N}_{t=1}\left|\hat{Y}_{i}-Y_{i}\right|
\end{aligned}
    \label{Eq:MAE}
\end{equation}

\textbf{Error de Porcentaje Absoluto (MAPE).} El MAPE es independiente. Sin embargo, MAPE fue criticado por el problema de la asimetría y la inestabilidad cuando el valor original es pequeño. MAPE como medida de precisión se ve afectada por cuatro problemas: 

\begin{enumerate}
\item Errores iguales por encima del valor real resultan en un mayor APE 
\item Se producen grandes porcentajes de errores cuando el valor de la serie original es pequeño
\item Los valores atípicos pueden distorsionar las comparaciones en estudios empíricos
\item MAPEs no se pueden comparar directamente con modelos ingenuos como el paseo al azar.  
\end{enumerate}
Su representación matemática se observa en la Ecuación \ref{Eq:MAPE}.

\begin{equation}
\begin{aligned}
  MAE = \frac{100}{n} \sum^{N}_{t=1}\left|\frac {\hat{Y}_{i}-Y_{i}}{\overline{y}}\right|
\end{aligned}
    \label{Eq:MAPE}
\end{equation}

\noindent \textbf{Horizonte de predicción.} El horizonte de predicción es el número de períodos futuros para los cuales se deben realizar pronósticos. El horizonte es a menudo dictado por la naturaleza del problema. Por ejemplo, en la planificación de la producción, los pronósticos de la demanda de productos pueden hacerse sobre una base mensual. Debido al tiempo necesario para cambiar o modificar un programa de producción, asegurarse de que la cadena de suministro disponga de suficiente materia prima y componentes y planifique la entrega de productos terminados a los clientes o instalaciones de inventario, sería necesario pronosticar hasta tres meses por delante. El horizonte de pronóstico también se conoce con frecuencia como el plazo de pronóstico.\\

\noindent \textbf{Intérvalo de predicción.} El intervalo de predicción es la frecuencia con la que se preparan nuevos pronósticos. Por ejemplo, en la planificación de la producción, podríamos pronosticar la demanda mensualmente, hasta por tres meses en el futuro (el plazo u horizonte) y preparar un nuevo pronóstico cada mes. Por lo tanto, el intervalo de pronóstico es de un mes, el mismo que el período básico de tiempo para el cual se hace cada pronóstico. Si el plazo de pronóstico es siempre la misma duración, por ejemplo, T períodos, y el pronóstico se revisa cada período de tiempo, entonces estamos empleando un enfoque de pronóstico de horizonte móvil o móvil.\\

%\noindent \textbf{Patrones de comportamiento.}\\

%\begin{itemize}
%  \item Tendencia lineal.
%  \item Ciclicidad.
%  \item Temporalidad.
%  \item No estacionario.
%  \item Eventos atípicos.
%\end{itemize}

%\newpage

\section{El Proceso de Pronóstico}
La actividad de generación de pronósticos consiste en un conjunto de pasos que comprenden el proceso del pronóstico. Las actividades que comprenden este proceso son las siguientes:

\begin{enumerate}
  \item Definición del problema
  \item Recolección de datos
  \item Análisis de datos
  \item Selección y ajuste del modelo 
  \item Validación del modelo
  \item Implementación del modelo de pronóstico
  \item Supervisión del rendimiento del modelo de pronóstico
\end{enumerate}

Como se puede observar en la figura \ref{fc_process} el proceso se comprende de una serie de actividades.
\begin{figure}[h]
  \centering
    \includegraphics[scale=0.52]{Capitulo2/figs/proceso_pronostico.pdf}
  \caption{El proceso del pronóstico.}            %Pie de imagen
  \label{fc_process}                              %nombre de referencia
\end{figure}


\subsection{Definición del problema}
La definición del problema implica desarrollar la comprensión de cómo el pronóstico será utilizado junto con las expectativas del cliente (el usuario del pronóstico). Las preguntas que deben abordarse durante esta fase incluyen la forma deseada de el pronóstico (por ejemplo, se requieren pronósticos mensuales). El horizonte del pronóstico o el plazo de entrega. Con qué frecuencia es necesario revisar el pronóstico (el intervalo de pronóstico). Y qué nivel de exactitud de pronóstico se requiere para tomar buenas decisiones empresariales. Esta es también una oportunidad para introducir a los responsables de la toma de decisiones en el uso de intervalos de predicción como una medida del riesgo asociado con el pronóstico. Si no están familiarizados con este enfoque. A menudo es necesario profundizar en muchos aspectos del sistema empresarial que requiere que el pronóstico defina correctamente el componente de pronóstico de todo el problema. Por ejemplo, al diseñar un sistema de pronóstico para el control de inventario. Puede ser necesaria información sobre cuestiones tales como la vida útil del producto u otras consideraciones de envejecimiento. El tiempo requerido para fabricar o de otra manera obtener los productos (plazo de producción). Y las consecuencias económicas de tener demasiadas o muy pocas unidades de producto disponibles para satisfacer la demanda de los clientes. Cuando varios productos están involucrados. El nivel de agregación del pronóstico (por ejemplo, predecir productos individuales o familias que consisten en varios productos similares) puede ser importante. Gran parte del éxito final del modelo de pronóstico para satisfacer las expectativas del cliente se determina en la fase de definición del problema.\\

\subsection{Recolección de datos}
La recolección de datos consiste en obtener el historial relevante para la variable o variables que se van a pronosticar, incluyendo la información histórica sobre las variables predictoras potenciales. La clave aquí es "relevante"; A menudo la recopilación de información y los métodos y sistemas de almacenamiento cambian con el tiempo y no todos los datos históricos son útiles para el problema actual. A menudo es necesario tratar los valores faltantes de algunas variables, posibles valores atípicos u otros problemas relacionados con los datos que se han producido en el pasado. Durante esta fase también es útil comenzar a planear cómo se manejarán los problemas de recopilación y almacenamiento de datos en el futuro para que se preserve la fiabilidad e integridad de los datos.\\

\subsection{Análisis de datos}
El análisis de datos es un paso preliminar importante para la selección del modelo de predicción a utilizar. Gráficas de series de tiempo de los datos deben ser construidas e inspeccionadas visualmente para patrones reconocibles, tales como tendencias y componentes estacionales u otros componentes cíclicos. Una tendencia es el movimiento evolutivo, ya sea hacia arriba o hacia abajo, en el valor de la variable. Las tendencias pueden ser a largo plazo o más dinámicas y de duración relativamente corta. La estacionalidad es el componente del comportamiento de la serie temporal que se repite de forma regular, como cada año. A veces, suavizaremos los datos para que la identificación de los patrones sea más obvia. También se deben calcular y evaluar los resúmenes numéricos de los datos, tales como la media de la muestra, la desviación estándar, los percentiles y las autocorrelaciones. Si se dispone de variables predictoras potenciales, deben examinarse las gráficas de dispersión de cada par de variables. Los puntos de datos inusuales o los valores atípicos potenciales deben ser identificados y marcados para posible estudio adicional. El propósito de este análisis preliminar de datos es obtener una cierta "sensación" para los datos, y una sensación de cómo fuertes los patrones subyacentes tales como tendencia y estacionalidad son. Esta información suele sugerir los tipos iniciales de métodos y modelos cuantitativos de predicción a explorar.\\

\subsection{Selección y ajuste del modelo}
La selección y ajuste del modelo consiste en elegir uno o más modelos de predicción y ajustar el modelo a los datos. Por ajuste, queremos decir la estimación de los parámetros del modelo desconocido, generalmente por el método de mínimos cuadrados. En los capítulos siguientes, presentaremos varios tipos de modelos de series de tiempo y discutiremos los procedimientos de ajuste del modelo. También discutiremos métodos para evaluar la calidad del ajuste del modelo y determinar si alguna de las suposiciones subyacentes ha sido violada. Esto será útil para discriminar entre los diferentes modelos de candidatos.\\

\subsection{Validación del modelo}
La validación del modelo consiste en una evaluación del modelo de pronóstico para determinar cómo es probable que se realice en la aplicación pronosticadora. Esto debe ir más allá de la simple evaluación del "ajuste" del modelo a los datos históricos y debe examinar qué magnitud de errores de predicción se experimentarán cuando el modelo se utilice para pronosticar datos "frescos" o nuevos. Los errores de ajuste serán siempre menores que los errores de pronóstico, y este es un concepto importante que enfatizaremos en este libro. Un método ampliamente utilizado para validar un modelo de pronóstico antes de ser entregado al cliente es emplear alguna forma de división de datos, donde los datos se dividen en dos segmentos: un segmento de ajuste y un segmento de pronóstico. El modelo se ajusta sólo al segmento de datos de ajuste y, a continuación, se simulan los pronósticos de ese modelo para las observaciones en el segmento de pronóstico. Esto puede proporcionar una guía útil sobre cómo funcionará el modelo de pronóstico cuando se exponga a nuevos datos y puede ser un enfoque valioso para discriminar entre modelos de predicción competitivos.\\

\subsection{Implementación del modelo de pronóstico}
El despliegue del modelo de pronóstico implica obtener el modelo y los pronósticos resultantes en uso por parte del cliente. Es importante asegurarse de que el cliente entiende cómo utilizar el modelo y que la generación de pronósticos oportunos desde el modelo se vuelve lo más rutinario posible. El mantenimiento del modelo, incluido el aseguramiento de que las fuentes de datos y la otra información requerida seguirán estando disponibles para el cliente, es también un tema importante que afecta la puntualidad y la utilidad final de los pronósticos.\\

\subsection{Supervisión del rendimiento del modelo de pronóstico}
El desempeño del modelo de pronóstico de monitoreo debe ser una actividad continua después de que el modelo se ha implementado para asegurar que todavía está funcionando. Es la naturaleza de la predicción que las condiciones cambian con el tiempo y un modelo que se desempeñó bien en el pasado puede deteriorarse en el desempeño. Por lo general, el deterioro del rendimiento resultará en errores de pronóstico más grandes o más sistemáticos. Por lo tanto, el monitoreo de errores previos es una parte esencial del buen diseño del sistema de pronósticos. Los gráficos de control de los errores de pronóstico son una manera simple pero efectiva de monitorear rutinariamente el desempeño de un modelo de pronóstico. Ilustraremos los enfoques para monitorear los errores de pronóstico en los capítulos siguientes.\\

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%           Métodos estadísticos                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Métodos Estadísticos de Pronóstico}
Es una disciplina que requiere de quien la practica una serie de distintas habilidades
para la obtención de los requisitos con los que debe de contar un sistema a desarrollar. La ingeniería de requisitos tiene que ver con
todo lo relacionado al descubrimiento, educción, desarrollo, análisis, determinación de métodos de verificación, validación,
comunicación, documentación y administración de requerimientos \cite{Standard2011}.\\


De acuerdo con Brooks en su ensayo ``No silver bullet - Essence and Accidents of Software Engineering'' el llevar a cabo el proceso de ingeniería de
requisitos es la parte más complicada del proceso de desarrollo de software:\\

``La tarea más difícil sobre el desarrollo es llegar a una completa y
consistente especificación, y mucha de la esencia de construir un programa
es de hecho la depuración de la especificación''\cite{brooksNoSilverBullet}.\\

El proceso de ingeniería de requisitos requiere del ingeniero de software
una serie de habilidades que van desde lo social hasta lo crítico-analítico. Entre las habilidades deseables
de un ingeniero de requisitos se encuentra la capacidad para comunicarse efectivamente con las personas
mediante entrevistas, resolver conflictos entre distintos stakeholders, la capacidad de analizar y observar críticamente a los diferentes
usuarios finales del sistema para poder derivar requisitos a partir de sus actividades comunes; además requiere de una
gran facilidad de organización y habilidades para poder plasmar y modelar los requisitos que se obtienen durante el proceso.\\

\noindent \textbf{Na\"ive.}

\noindent \textbf{AR.}
De la definición anterior es posible observar que los requisitos pueden pertenecer a distintas clasificaciones. Los requisitos
se clasifican en requisitos funcionales y requisitos no funcionales.\\

\noindent \textbf{ARMA.}

\noindent \textbf{ARIMA.}

\noindent \textbf{Holt Winters.}


\section{Métodos de Iteligencia Artificial}

\noindent La historia ha mostrado que los avances tecnológicos se mueven continuamente, que cada corto periodo de tiempo surgen
nuevas tecnologías y con ello nuevas tendencias y maneras de realizar las actividades cotidianas.\\

\noindent \textbf{Inteligencia artificial (IA).} Un gran volumen de datos serán almacenados en los centros de datos. Dependiendo del nivel de complejidad que tenga la aplicación de la IoT
que se esté desarrollando puede ser necesario que toda esta información deba ser minada para poder generar conocimiento y automatizar acciones en base a ello.

\noindent \textbf{RNA.} Son las salidas de los dispositivos, estas salidas se pueden reflejar en forma de motores, luces de led, etc. Es decir,
hacen que el dispositivo interactúe con el mundo real.\\

\begin{figure}[h]
  \centering
    \includegraphics[scale=0.65]{Capitulo2/figs/fig_ANN_es_new.pdf}
  \caption{Red neuronal artificial}            %Pie de imagen
  \label{fig_ANN_es_new}                              %nombre de referencia
\end{figure}

\noindent \textbf{SVM.} Las tecnologías web brindan diversas herramientas que facilitan la administración remota en la IoT.
Permiten que desde cualquier lugar y en cualquier momento se pueda tener acceso a la información de algún sistema en tiempo real, tal sería el caso de una
casa inteligente, sistemas de riego, etc.\\

\noindent \textbf{KNN.} Las tecnologías web brindan diversas herramientas que facilitan la administración remota en la IoT.
Permiten que desde cualquier lugar y en cualquier momento se pueda tener acceso a la información de algún sistema en tiempo real, tal sería el caso de una
casa inteligente, sistemas de riego, etc.\\

\noindent \textbf{AG.} La computación en la nube es un cambio de paradigma computacional el cual lleva la mayor parte del procesamiento
y almacenamiento de datos a centros de datos\cite{Dikaiakos2009}. Este cambio de paradigma ha permitido el desarrollo de nuevos productos como el software como servicio y es este
mismo paradigma el que permite que exista un gran flujo de datos proveniente de los diversos dispositivos que sean provistos sensores y actuadores.\\

\section{Métodos Híbridos}




\section{Conclusiones}

En este capítulo se ha hablado sobre un marco conceptual de ingeniería de software e IoT. Como ha podido ser observado,
la utilización de sensores y actuadores es indispensable para este tipo de aplicaciones en las que el mundo físico es extendido para interactuar con el mundo digital,
motivo que convierte en esencial a la disciplina de la ingeniería electrónica para el desarrollo de este tipo de proyectos. Por otro lado se encuentra
la parte computacional de estas aplicaciones, que puede llegar a ser muy extensa por requerir desde técnicas de ingeniería de software para la administración, diseño, construcción y desarrollo de proyectos
hasta incluso el análisis de grandes volúmenes de datos usando técnicas de inteligencia artificial.











































